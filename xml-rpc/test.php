<?php

require_once 'app/Mage.php';

$yourApiUser = 'xxxxx';
$yourApiKey = 'xxxxx';

// Get an XMLRPC client.
$client = new Zend_XmlRpc_Client('https://spiritedgifts.com/index.php/api/xmlrpc/');

// For testing, just set an infinite timeout.
$client->getHttpClient()->setConfig(array('timeout' => -1));

// Login to the API and get a session token.
$session = $client->call('login', array($yourApiUser, $yourApiKey));

/*
// Import regular entities (products, categories, customers).
$client->call('call', array(
    $session,
    'import.importEntities',
    array($anArrayWithYourEntities, $entityType, $optionalImportBehavior)
));
*/

// End our session.
$client->call('endSession', array($session));
