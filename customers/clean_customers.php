<?php

class Cleaner
{
    private $websiteId;

    private $store;
    /**
     * @var Mage_Customer_Model_Resource_Customer_Collection
     */
    private $customersCollection;

    private $filters = array();

    private $limit = 100;

    private $dryRun = true;

    private $customerMode = true;

    private function prepareFilters()
    {
        if (!is_array($this->filters) || !count($this->filters)) {
            return false;
        }
        foreach ($this->filters as $name => $filter) {
            // special macros
            if (is_array($filter) && isset($filter['macros']) && is_array($filter['macros'])) {
                $macros = $filter['macros'];
                $operator = @$macros['operator'];
                $action = @$macros['action'];
                $param = @$macros['param'];
                switch ($action) {
                    case 'file_list':
                        $value = $this->loadFilter($param);
                        if ($value) {
                            $filter[$operator] = $value;
                        } else {
                            return false;
                        }
                        break;
                }
            }
            if ($this->customerMode) {
                $this->customersCollection->addAttributeToFilter($name, $filter);
            } else {
                $this->customersCollection->addFieldToFilter($name, $filter);
            }
        }
        return true;
    }

    public function init($filters = array(), $dryRun = true, $limit = 100, $customerMode = true)
    {
        $result = false;
        $this->websiteId = Mage::app()->getWebsite()->getId();
        $this->store = Mage::app()->getStore();
        $this->dryRun = $dryRun;
        $this->limit = $limit;
        $this->customerMode = $customerMode;

        $this->filters = $filters;
        if ($this->customerMode) {
            $customerModel = Mage::getModel('customer/customer');
        } else {
            $customerModel = Mage::getModel('newsletter/subscriber');
        }
        $this->customersCollection = $customerModel->getCollection();
        if (!$this->prepareFilters()) {
            return $result;
        }
        if ($this->limit) {
            $this->customersCollection->setPageSize($this->limit);
        }

        $this->customersCollection->load(true, true);
        if ($this->customersCollection->count()) {
            $result = true;
            echo PHP_EOL . 'Collection loaded: ' . $this->customersCollection->count() . PHP_EOL;
        }
        return $result;
    }

    public function loadFilter($path)
    {
        $result = false;
        try {
            if (@file_exists($path)) {
                $content = file($path);
                if (is_array($content) && count($content)) {
                    $result = array_map('trim', $content);
                }
            }
        } catch (Exception $exception) {}
        return $result;
    }

    public function execute($processors = array())
    {
        if (!is_array($processors) || !count($processors)) {
            return false;
        }
        $results = array();
        $index = 0;
        foreach ($processors as $index => $paramsSet) {
            if (is_array($paramsSet)) {
                foreach ($paramsSet as $processor => $params) {
                    $index++;
                    $key = $index . '. ' . $processor;
                    $methodName = 'process' . $processor;
                    if (method_exists($this, $methodName)) {
                        try {
                            $buffer = $this->{$methodName}($params);
                            if (!is_array($buffer)) {
                                $results[$key] = array(
                                    'status' => false,
                                    'content' => $buffer
                                );
                            } else {
                                $results[$key] = $buffer;
                            }
                        } catch (Exception $exception) {
                            $results[$key] = array(
                                'status' => false,
                                'content' => $exception->getMessage()
                            );
                        }
                    }
                }
            }
        }
        return $results;
    }

    /**
     * @param Mage_Customer_Model_Customer $customer|Mage_Newsletter_Model_Subscriber
     * @return bool|Mage_Newsletter_Model_Subscriber
     */
    public function getSubscriber($customer)
    {
        if ($this->customerMode) {
            $subscriber = Mage::getModel('newsletter/subscriber')->loadByCustomer($customer);
            return ($subscriber && $subscriber->getId()) ? $subscriber : false;
        } else {
            return $customer;
        }
    }

    public function arrayToCsv(array &$fields, $delimiter = ';', $enclosure = '"', $encloseAll = false, $nullToMysqlNull = false) {
        $delimiter_esc = preg_quote($delimiter, '/');
        $enclosure_esc = preg_quote($enclosure, '/');

        $output = array();
        foreach ($fields as $field) {
            if ($field === null && $nullToMysqlNull) {
                $output[] = 'NULL';
                continue;
            }

            // Enclose fields containing $delimiter, $enclosure or whitespace
            if ($encloseAll || preg_match( "/(?:${delimiter_esc}|${enclosure_esc}|\s)/", $field )) {
                $output[] = $enclosure . str_replace($enclosure, $enclosure . $enclosure, $field) . $enclosure;
            }
            else {
                $output[] = $field;
            }
        }

        return implode($delimiter, $output);
    }

    public function logSelection($path = 'var/log/cleaner/clean_customers_selection-%s.txt')
    {
        $result = array();
        $map = array(
            'email' => 'getEmail',
            'first_name' => 'getFirstName',
            'last_name' => 'getLastName',
            'id' => 'getId',
            'created_at' => 'getCreatedAt'
        );
        foreach ($this->customersCollection->getItems() as $key => $customer) {
            $result[$key] = array(
                '__key' => $key
            );
            foreach ($map as $to => $method) {
                try {
                    $result[$key][$to] = $customer->{$method}();
                } catch (Exception $exception) {}
            }
        }
        $csvContent = $this->arrayToCsv(array_keys($map)) . PHP_EOL;
        foreach ($result as $row) {
            $csvContent .= $this->arrayToCsv($row) . PHP_EOL;
        }
        $path = sprintf($path, date('Y-m-d_H-i-s'));
        return file_put_contents($path, $csvContent);
    }

    public function processDeleteCustomer($params)
    {
        $result = array();
        $skipGroups = isset($params['skip_groups']) ? $params['skip_groups'] : array();
        foreach ($this->customersCollection->getItems() as $key => $customer) {
            /** @var Mage_Customer_Model_Customer $customer */
            $email = $customer->getEmail();
            $group = $customer->getGroupId();
            if (in_array($group, $skipGroups)) {
                static::printLog($result, sprintf('[%s] - DELETE CUSTOMER: slip by group id = `%s`.', $email, $group));
                continue;
            }
            try {
                $customer->load($customer->getId());
                if (!$this->dryRun) {
                    $customer->delete();
                }
                static::printLog($result, sprintf('[%s] - DELETE CUSTOMER: customer with id `%s` was deleted.', $email, $key));
            } catch (Exception $exception) {
                static::printLog($result, sprintf('[%s] - DELETE CUSTOMER: error while deleting customer with id: `%s`.', $email, $key));
            }
        }
        $out = array(
            'status' => true,
            'content' => implode(PHP_EOL, $result)
        );
        return $out;
    }

    public function processSkipByOrders($params)
    {
        $result = array();
        $skip = array();
        foreach ($this->customersCollection->getItems() as $key => $customer) {
            /** @var Mage_Sales_Model_Resource_Order_Collection $ordersCollection */
            $ordersCollection = Mage::getModel('sales/order')->getCollection();
            $email = $customer->getEmail();
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $ordersCollection->addFieldToFilter('customer_email', array(
                    'eq' => $email
                ));
                if ($ordersCollection->count()) {
                    $skip[$email] = $key;
                } else {
                    static::printLog($result, sprintf('[%s] - SKIP BY ORDER: no orders.', $email));
                }
            } else {
                static::printLog($result, sprintf('[%s] - SKIP BY ORDER: not valid email.', $email));
            }
        }
        foreach ($skip as $email => $key) {
            try {
                $this->customersCollection->removeItemByKey($key);
                static::printLog($result, sprintf('[%s] - SKIP BY ORDER: Key `%s` was skipped.', $email, $key));
            } catch (Exception $exception) {
                static::printLog($result, sprintf('[%s] - SKIP BY ORDER: can not skip key `%s`', $email, $key));
            }
        }
        $out = array(
            'status' => true,
            'content' => implode(PHP_EOL, $result)
        );
        return $out;
    }

    public function processUnsubscribe($params = array())
    {
        $result = array();
        if (isset($params['mode'])) {
            $mode = $params['mode'];
            foreach ($this->customersCollection->getItems() as $model) {
                $subscriber = $this->getSubscriber($model);
                if ($subscriber) {
                    try {
                        if (!$this->dryRun) {
                            switch ($mode) {
                                case 'unsubscribe':
                                    $model->setSubscriberStatus(Mage_Newsletter_Model_Subscriber::STATUS_UNSUBSCRIBED)
                                        ->save();
                                    break;
                                case 'delete':
                                    $model->delete();
                                    break;
                            }
                        }
                        static::printLog($result, sprintf(
                            '[%s] - UNSUBSCRIBE M1: action `%s` was performed.',
                            $model->getEmail(),
                            $mode
                        ));
                    } catch (Exception $exception) {
                        static::printLog($result, sprintf(
                            '[%s] - UNSUBSCRIBE M1: error on action `%s`: ' . $exception->getMessage(),
                            $model->getEmail(),
                            $mode
                        ));
                    }
                } else {
                    static::printLog($result, sprintf(
                        '[%s] - UNSUBSCRIBE M1: skip action `%s` as not subscribed.',
                        $model->getEmail(),
                        $mode
                    ));
                }
            }
        }
        $out = array(
            'status' => true,
            'content' => implode(PHP_EOL, $result)
        );
        return $out;

    }

    public function processMailChimp($params = array())
    {
        $result = array();
        if (isset($params['mode'])) {
            if (!class_exists('Ebizmarts_MailChimp_Model_Api_subscribers')) {
                return 'Class `Ebizmarts_MailChimp_Model_Api_subscribers` does not exists';
            }
            $mode = $params['mode'];
            $api = new Ebizmarts_MailChimp_Model_Api_subscribers();
            foreach ($this->customersCollection->getItems() as $customer) {
                $subscriber = $this->getSubscriber($customer);
                if ($subscriber) {
                    try {
                        if (!$this->dryRun) {
                            switch ($mode) {
                                case 'unsubscribe':
                                    $api->removeSubscriber($subscriber);
                                    break;
                                case 'delete':
                                    $api->deleteSubscriber($subscriber);
                                    break;
                            }
                        }
                        static::printLog($result, sprintf(
                            '[%s] - MAILCHIMP: action `%s` was performed.',
                            $customer->getEmail(),
                            $mode
                        ));
                    } catch (Exception $exception) {
                        static::printLog($result, sprintf(
                            '[%s] - MAILCHIMP: error on action `%s`: ' . $exception->getMessage(),
                            $customer->getEmail(),
                            $mode
                        ));
                    }
                } else {
                    static::printLog($result, sprintf(
                        '[%s] - MAILCHIMP: skip action `%s` as not subscribed.',
                        $customer->getEmail(),
                        $mode
                    ));
                }
            }
        }
        $out = array(
            'status' => true,
            'content' => implode(PHP_EOL, $result)
        );
        return $out;
    }

    public static function printLog(&$result, $message)
    {
        $result[] = $message;
        echo date('Y/m/d H:i:s') . ' ' . $message . PHP_EOL;
    }

    public function logProcessors($log, $path = 'var/log/cleaner/log_processors-%s.txt')
    {
        $result = '';
        $header = <<<HEADER

--------------------------------------
----- %s -----
--------------------------------------

STATUS: %s

%s
HEADER;
        if (is_array($log)) {
            foreach ($log as $processor => $data) {
                $result .= sprintf($header, $processor, @$data['status'], @$data['content']);
            }
        }
        $path = sprintf($path, date('Y-m-d_H-i-s'));
        return file_put_contents($path, $result);
    }
}



//increase execution time
ini_set('max_execution_time', 900); //900 seconds = 15 minutes

//require Magento
require_once 'app/Mage.php';
$app = Mage::app('admin');
umask(0);

//enable Error Reporting
error_reporting(E_ALL & ~E_NOTICE);

try{
    $filters = array(
        /*
        'phone' => array(
            'like' => '1234567'
        ),
        'zip' => array(
            'like' => '123456'
        ),

        'company' => array(
            'like' => 'Company'
        ),
        */
        'subscriber_email' => array(
            'macros' => array(
                'operator' => 'in',
                'action' => 'file_list',
                'param' => 'left_bad_accounts_left.txt'
            )
        ),
        /*
        'email' => array(
            array(
                'like' => '%@qq.com'
            ),
            array(
                'like' => '%@QQ.COM'
            ),
            array(
                'like' => '%@QQ.com'
            ),
            array(
                'like' => '%@qq.COM'
            ),
            array(
                'like' => '%@qQ.com'
            ),
            array(
                'like' => '%@Qq.com'
            ),
            array(
                'like' => '%@126.com'
            ),
            array(
                'like' => '%@139.com'
            ),
            array(
                'like' => '%@163.com'
            ),
            array(
                'like' => '%@qq.oj'
            ),
            array(
                'like' => '%@00172.com'
            ),
            array(
                'like' => '%21cn.com'
            ),

        ),*/
        /*
        'created_at' => array(
            // Jun 14, 2017 12:40:20 PM
            'from' => date('Y-m-d 00:00:00', strtotime('2017-06-14 10:10:10')),
            'to' => date('Y-m-d 23:59:59', strtotime('2017-06-19 23:59:59')),
        )*/
    );
    $processors = array(
        array(
            'SkipByOrders' => array(),
        ),
        array(
            'MailChimp' => array(
                'mode' => 'unsubscribe'
            )
        ),
        array(
            'MailChimp' => array(
                'mode' => 'delete',
            )
        ),
        array(
            'DeleteCustomer' => array(
                'skip_groups' => array()
            )
        )
        /*array(
            'Unsubscribe' => array(
                'mode' => 'unsubscribe'
            )
        ),
        array(
            'Unsubscribe' => array(
                'mode' => 'delete',
            )
        )*/
    );
    $cleaner = new Cleaner();
    if ($cleaner->init($filters, true, false, true)) {
        $cleaner->logSelection();
        $log = $cleaner->execute($processors);
        $cleaner->logProcessors($log);
    }
}
catch (Exception $e) {
    Zend_Debug::dump($e->getMessage());
}

