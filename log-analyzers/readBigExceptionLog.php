<?php

function parseReport()
{
    $exceptions = array();
    if (isset($_GET["submit"])) {
        $fileName = $_GET['filepath'];
        $handle = @fopen($fileName, "r");

        if ($handle) {
            $isNewEx = true;
            // Collect Exception
            while (($buffer = fgets($handle, 2000)) !== false) {
                $arr = explode("\n", trim($buffer));
                $buffer = substr($buffer, 22);
                if (strpos($arr[0], 'exception') !== false) {
                    //exception start
                    $isNewEx = true;
                    foreach ($exceptions as $k => $ex) {
                        if ($ex[0] == $buffer) {
                            $exceptions[$k][1] += 1;
                            $isNewEx = false;
                            break;
                        }
                    }
                    if ($isNewEx) {
                        $exceptions[] = array($buffer, 1);
                    }
                }
            }
            if (!feof($handle)) {
                echo "Error: unexpected fgets() fail\n";
            }
            fclose($handle);
        }
    }
    return $exceptions;
}

?>

<!DOCTYPE html>
<html>
<style>
    .exception_row {
        border: 1px solid #000000;
        padding: 2px;
    }
</style>
<body>

<form action="readBigExceptionLog.php" method="get" enctype="multipart/form-data">
    Local File Path:
    <input type="input" name="filepath" id="filepath">
    <input type="submit" value="Process" name="submit">
</form>
<?php foreach (parseReport() as $i => $ex): ?>
    <div class="exception_row">
        <?php echo "[{$i}] (Count:{$ex[1]}) - $ex[0]"; ?>
    </div>
<?php endforeach; ?>
</body>
</html>