<?php
// require the autoloader
$mage_path = dirname(__FILE__) . '/app/Mage.php';
require_once($mage_path);
$app = Mage::app();
$autoload_path = Mage::getBaseDir() . '/constant/Ctct/autoload.php';
require_once($autoload_path);

use Ctct\ConstantContact;
use Ctct\Components\Contacts\Contact;
use Ctct\Components\Contacts\ContactList;
use Ctct\Components\Contacts\EmailAddress;
use Ctct\Exceptions\CtctException;

// Enter your Constant Contact APIKEY and ACCESS_TOKEN
$config = Mage::getStoreConfig('constantcontact/settings');

if($config['constant_status'] == 0){
    return;
}
define("APIKEY", $config['constant_api_key']);
define("ACCESS_TOKEN", $config['access_token']);

$cc = new ConstantContact($config['constant_api_key']);

// get Subscribers list
$collection = Mage::getResourceModel('newsletter/subscriber_collection')->addFieldToFilter('subscriber_status', 1)->load();

foreach($collection->getItems() as $subscriber) {
    $customer = Mage::getModel('customer/customer');
    $email = $subscriber->getSubscriberEmail();
    $customer->setWebsiteId(Mage::app()->getStore()->getWebsiteId());
    $customer->loadByEmail($email);
    try {
        // check to see if a contact with the email addess already exists in the account
        $response = $cc->getContactByEmail(ACCESS_TOKEN, $email);

        // create a new contact if one does not exist
        if (empty($response->results)) {
            Mage::log('Creating Contact', null, 'agk.log');
            $contact = new Contact();
            $contact->addEmail($email);
            $contact->addList($config['list_id']);
            $contact->first_name = $customer->getData('firstname');
            $contact->last_name = $customer->getData('lastname');

            $returnContact = $cc->addContact(ACCESS_TOKEN, $contact, false);

            // update the existing contact if address already existed
        } /*else {
            Mage::log('Updating Contact', null, 'agk.log');
            $contact = $response->results[0];
            $contact->addList($config['list_id']);
            $contact->first_name = $customer->getData('firstname');
            $contact->last_name = $customer->getData('lastname');

            $returnContact = $cc->updateContact(ACCESS_TOKEN, $contact, false);
        }*/

        // catch any exceptions thrown during the process and print the errors to screen
    } catch (CtctException $ex) {
        Mage::log($ex->getErrors(), null, 'constantContact.log');
    }
}
?>