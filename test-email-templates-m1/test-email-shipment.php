<?php

/* (0) HACKS & FUNCTIONS :: START */
function renderPaymentBlockFromTestEmail($paymentBlock)
{
    return $paymentBlock->toHtml();
}
/* (0) HACKS & FUNCTIONS :: END */

/* (1) PARAMS :: START */
$orderId = 23794;
$customerId = 30364;
$storeIdSet = 1;
$emailTemplateFromConfig = 'sales_email/shipment/template'; // Mage_Sales_Model_Order::XML_PATH_EMAIL_TEMPLATE;
$copyTo = '';
$copyMethod = '';
$sender = array(
    'name' => 'test@aws3.com',
    'email' => 'test@aws3.com'
);
$to = array(
    'name' => 'test@aws3.com',
    'email' => 'test@aws3.com'
);
/* (2) PARAMS :: END */

/* (2) Launch Magento :: START */
date_default_timezone_set('America/New_York');
require_once('app/Mage.php');
Mage::app();
Mage::app()->setCurrentStore($storeIdSet);
/* (2) Launch Magento :: END */

/* (3) Set Main Variables :: START */
$store = Mage::app()->getStore();
$storeId = $store->getId();
/** @var Mage_Sales_Model_Order $order */
$order = Mage::getModel('sales/order')->load($orderId);
$customer = Mage::getModel('customer/customer')->load($customerId);
$customerName = $customer->getName();
$templateId = $emailTemplateFromConfig && !strlen($emailTemplateFromConfig) ? 0 : Mage::getStoreConfig($emailTemplateFromConfig);
$shipment = $order->getShipmentsCollection()->getFirstItem();
$history = $order->getStatusHistoryCollection()->getFirstItem();
$comment = $history->getComment();

// Start store emulation process
/** @var $appEmulation Mage_Core_Model_App_Emulation */
$appEmulation = Mage::getSingleton('core/app_emulation');
$initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

try {
    // Retrieve specified view block from appropriate design package (depends on emulated store)
    $paymentBlock = Mage::helper('payment')->getInfoBlock($order->getPayment())
        ->setIsSecureMode(true);
    $paymentBlock->getMethod()->setStore($storeId);
    $paymentBlockHtml = renderPaymentBlockFromTestEmail($paymentBlock);
} catch (Exception $exception) {
    // Stop store emulation process
    $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
    throw $exception;
}

// Stop store emulation process
$appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
/* (3) Set Main Variables :: END */

/* (4) Set Sender and Email Fields :: START */
/** @var $mailer Mage_Core_Model_Email_Template_Mailer */
$mailer = Mage::getModel('core/email_template_mailer');
/** @var $emailInfo Mage_Core_Model_Email_Info */
$emailInfo = Mage::getModel('core/email_info');
$emailInfo->addTo(@$to['email'], $customerName);
if ($copyTo && $copyMethod == 'bcc') {
    // Add bcc to customer email
    foreach ($copyTo as $email) {
        $emailInfo->addBcc($email);
    }
}
$mailer->addEmailInfo($emailInfo);
// Email copies are sent as separated emails if their copy method is 'copy'
if ($copyTo && $copyMethod == 'copy') {
    foreach ($copyTo as $email) {
        $emailInfo = Mage::getModel('core/email_info');
        $emailInfo->addTo($email);
        $mailer->addEmailInfo($emailInfo);
    }
}
// Set all required params and send emails
$mailer->setSender($sender);
$mailer->setStoreId($storeId);
$mailer->setTemplateId($templateId);
$mailer->setTemplateParams(array(
    'order'        => $order,
    'billing'      => $order->getBillingAddress(),
    'payment_html' => $paymentBlockHtml,
    'shipment'     => $shipment,
    'comment'      => $comment,
));
/* (4) Set Sender and Email Fields :: END */

/* (5) Like Send Email - Render :: START */
$emailTemplate  = Mage::getModel('core/email_template');
$emailTemplate->addBcc($emailInfo->getBccEmails());
$emailTemplate->setDesignConfig(array('area' => 'frontend', 'store' => $mailer->getStoreId()));
if (is_numeric($templateId)) {
    $emailTemplate->load($templateId);
} else {
    $localeCode = Mage::getStoreConfig('general/locale/code', $storeId);
    $emailTemplate->loadDefault($templateId, $localeCode);
}
$emailTemplate->load($mailer->getTemplateId());
$emailTemplate->setSenderName(@$mailer->getSender()['name']);
$emailTemplate->setSenderEmail(@$mailer->getSender()['email']);
$emailTemplate->setUseAbsoluteLinks(true);
/* (5) Like Send Email - Render :: END */

/* (6) Collect variables :: START */
$emailTemplateVars = $mailer->getTemplateParams();
$emailTemplateVars['usermessage'] = "blub";
$emailTemplateVars['store'] = Mage::app()->getStore();
$emailTemplateVars['sendername'] = @$mailer->getSender()['email'];
$emailTemplateVars['receivername'] = @$to['email'];
$emailTemplateVars['store'] = Mage::app()->getStore($storeId);
$emailTemplateVars['email'] = @$to['email'];
$emailTemplateVars['name'] = @$to['email'];
/* (6) Collect variables :: END */

$text = $emailTemplate->getProcessedTemplate($emailTemplateVars, true);
echo $text;
