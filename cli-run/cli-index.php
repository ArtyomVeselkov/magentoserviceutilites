<?php

define('CONSOLE_CLI_ACCESS_KEY', 1);

$pref_root = getcwd();
$pref_domain = 'vannabelt.loc';
$_SERVER = array
(
    'REDIRECT_HTTP_AUTHORIZATION' => ' ',
    'REDIRECT_STATUS' => '200',
    'HTTP_AUTHORIZATION' => '',
    'HTTP_HOST' => $pref_domain,
    'HTTP_CONNECTION' => 'keep-alive',
    'HTTP_UPGRADE_INSECURE_REQUESTS' => '1',
    'HTTP_USER_AGENT' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36 Vivaldi/1.2.490.43',
    'HTTP_ACCEPT' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'HTTP_ACCEPT_ENCODING' => 'gzip, deflate, sdch',
    'HTTP_ACCEPT_LANGUAGE' => 'en-US,en;q=0.8,ru;q=0.6',
    'HTTP_COOKIE' => 'm2e_bn_6251bd2848=1; m2e_bn_5300762365=1; m2e_bn_f25faa424a=1; m2e_bn_bc22baa29e=1; m2e_bn_398801d0fc=1; m2e_bn_956e9bb04d=1; XDEBUG_SESSION=PHPSTORM; PHPSESSID=lm6g1d1892at5eovnhlkidqs75; persistent_shopping_cart=k9c8Cif2PX6rtM0LhC1z72VcB1UHads0EjSrZyk8mTt0j5KAbu; _ga=GA1.2.1824746196.1469206568; __ar_v4=VRJ4MI7WVNDDDIFECDRBPG%3A20160723%3A23%7CFCVROADRGFDW5OATGO6M4S%3A20160721%3A15%7CKHF4YT246VF6DI4JRUFV66%3A20160721%3A38%7CFSW7524KKZDU7FSR33LCAF%3A20160721%3A38',
    'PATH' => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
    'SERVER_SIGNATURE' => "<address>Apache/2.4.18 (Ubuntu) Server at $pref_domain Port 80</address>",

    'SERVER_SOFTWARE' => 'Apache/2.4.18 (Ubuntu)',
    'SERVER_NAME' => $pref_domain,
    'SERVER_ADDR' => '127.0.1.1',
    'SERVER_PORT' => '80',
    'REMOTE_ADDR' => '127.0.0.1',
    'DOCUMENT_ROOT' => $pref_root,
    'REQUEST_SCHEME' => 'http',
    'CONTEXT_PREFIX' => ' ',
    'CONTEXT_DOCUMENT_ROOT' => $pref_root,
    'SERVER_ADMIN' => 'webmaster@localhost',
    'SCRIPT_FILENAME' => $pref_root . 'index.php',
    'REMOTE_PORT' => '60058',
    'REDIRECT_URL' => '/',
    'GATEWAY_INTERFACE' => 'CGI/1.1',
    'SERVER_PROTOCOL' => 'HTTP/1.1',
    'REQUEST_METHOD' => 'GET',
    'QUERY_STRING' => '',
    'REQUEST_URI' => '/',
    'SCRIPT_NAME' => '/index.php',
    'PHP_SELF' => '/index.php',
    'REQUEST_TIME_FLOAT' => '1469445801.338',
    'REQUEST_TIME' => '1469445801',
);

include_once 'index.php';
