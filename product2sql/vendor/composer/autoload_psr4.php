<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'EPDOStatement\\' => array($vendorDir . '/noahheck/e_pdostatement/src'),
);
