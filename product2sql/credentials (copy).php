<?php
    $credentialsAliased = array(
        'host' => 'localhost',
        'user' => 'root',
        'password' => 'root',
        'db' => 'vbm2c1'
    );

    // in order to keep right order
    return array(
        $credentialsAliased['host'],
        $credentialsAliased['user'],
        $credentialsAliased['password'],
        $credentialsAliased['db']
    );