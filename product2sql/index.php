<?php

namespace Aws\Utilites;

/**
 * TODO - add prefix support
 * TODO - add strict logic for some tables
 * TODO - make one array for both Magento Version
 *
 */

require_once "vendor/autoload.php";
use Aws\DataMigrationSubTool\Exception;
use EPDOStatement\EPDOStatement;
use Magento\Shipping\Model\Carrier\AbstractCarrier;

class ProductExport2Sql
{
    const DEFAULT_OUTPUT_FILE = 'output/export_products.sql';
    const DEFAULT_INSERT_MODE = 'REPLACE';
    const DEFAULT_MAGENTO_VER = 'm2';
    const DEFAULT_WITH_PK_UK = true;

    protected $epilogueDisableChecks = array(
        'before' => <<<EOT
SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT;
SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS;
SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION;
SET NAMES utf8;
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';
SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0; 
EOT
    ,
        'after' => <<<EOT
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT;
SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS;
SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION;
SET SQL_NOTES=@OLD_SQL_NOTES;
EOT
    );



    protected $exportTablesM1 = array(
        "catalog_product_bundle_option" => array(
            'keys' => 'parent_id',
            'join' => array(
                'catalog_product_bundle_option_value' => 'option_id:option_id'
            )
        ),

        "catalog_product_bundle_price_index" => 'entity_id',
        "catalog_product_bundle_selection" => 'product_id',
        // join
        // "catalog_product_bundle_selection_price" => 'entity_id',

        "catalog_product_bundle_stock_index" => 'entity_id',
        "catalog_product_entity" => 'entity_id',
        "catalog_product_entity_datetime" => 'entity_id',
        "catalog_product_entity_decimal" => 'entity_id',
        "catalog_product_entity_gallery" => 'entity_id',
        "catalog_product_entity_int" => 'entity_id',
        "catalog_product_entity_media_gallery" => 'entity_id',
        // join or unneeded
        // "catalog_product_entity_media_gallery_value" => 'entity_id',
        // "catalog_product_entity_media_gallery_value_to_entity" => 'entity_id',
        // "catalog_product_entity_media_gallery_value_video" => 'entity_id',

        "catalog_product_entity_text" => 'entity_id',
        "catalog_product_entity_tier_price" => 'entity_id',
        "catalog_product_entity_varchar" => 'entity_id',
        "catalog_product_index_eav" => 'entity_id',

        "catalog_product_index_price" => 'entity_id',

        "catalog_product_index_eav_decimal" => 'entity_id',

        //"catalog_product_index_eav_decimal_idx" => 'entity_id',
        //"catalog_product_index_eav_decimal_tmp" => 'entity_id',
        //"catalog_product_index_eav_idx" => 'entity_id',
        //"catalog_product_index_eav_tmp" => 'entity_id',
        //"catalog_product_index_price_bundle_idx" => 'entity_id',
        //"catalog_product_index_price_bundle_opt_idx" => 'entity_id',
        //"catalog_product_index_price_bundle_opt_tmp" => 'entity_id',
        //"catalog_product_index_price_bundle_sel_idx" => 'entity_id',
        //"catalog_product_index_price_bundle_sel_tmp" => 'entity_id',
        //"catalog_product_index_price_bundle_tmp" => 'entity_id',
        //"catalog_product_index_price_cfg_opt_agr_idx" => 'entity_id',
        //"catalog_product_index_price_cfg_opt_agr_tmp" => 'entity_id',
        //"catalog_product_index_price_cfg_opt_idx" => 'entity_id',
        //"catalog_product_index_price_cfg_opt_tmp" => 'entity_id',
        //"catalog_product_index_price_downlod_idx" => 'entity_id',
        //"catalog_product_index_price_downlod_tmp" => 'entity_id',
        //"catalog_product_index_price_final_idx" => 'entity_id',
        //"catalog_product_index_price_final_tmp" => 'entity_id',
        //"catalog_product_index_price_idx" => 'entity_id',
        //"catalog_product_index_price_opt_agr_idx" => 'entity_id',
        //"catalog_product_index_price_opt_agr_tmp" => 'entity_id',
        //"catalog_product_index_price_opt_idx" => 'entity_id',
        //"catalog_product_index_price_opt_tmp" => 'entity_id',
        //"catalog_product_index_price_tmp" => 'entity_id',

        "catalog_product_index_tier_price" => 'entity_id',

        // no need, no product info
        // "catalog_product_index_website" => 'entity_id',

        "catalog_product_link" => 'product_id',
        // seems no need
        // "catalog_product_link_attribute" => 'entity_id',

        // join
        // "catalog_product_link_attribute_decimal" => 'entity_id',
        // "catalog_product_link_attribute_int" => 'entity_id',
        // "catalog_product_link_attribute_varchar" => 'entity_id',

        // no need
        // "catalog_product_link_type" => 'entity_id',

        "catalog_product_option" => 'product_id',

        // join
        // "catalog_product_option_price" => 'entity_id',
        // "catalog_product_option_title" => 'entity_id',
        // "catalog_product_option_type_price" => 'entity_id',
        // "catalog_product_option_type_title" => 'entity_id',
        // "catalog_product_option_type_value" => 'entity_id',

        "catalog_product_relation" => array(
            'keys' => array('parent_id', 'child_id')
        ),

        "catalog_product_super_attribute" => array(
            'keys' => array('product_id'),
            'join' => array(
                'catalog_product_super_attribute_label' => 'product_super_attribute_id:product_super_attribute_id',
                'catalog_product_super_attribute_pricing' => 'product_super_attribute_id:product_super_attribute_id'
            )
        ),

        "catalog_product_super_link" => array(
            'keys' => array('product_id', 'parent_id')
        ),

        'cataloginventory_stock_item' => 'product_id',
        'cataloginventory_stock_status' => 'product_id',

        // 'cataloginventory_stock_status_idx',
        // 'cataloginventory_stock_status_tmp',

        /**
         * review need to add additional functionality -- e.g. entity_id MUST BE equal '1' (product)
         */

        'tag_relation' => array(
            'keys' => 'product_id',
            'join' => array(
                'tag' => 'tag_id:tag_id',
                'tag_properties' => 'tag_id:tag_id',
                'tag_summary' => 'tag_id:tag_id'
            )
        ),

        'catalog_product_website' => 'product_id',
        'catalog_category_product' => 'product_id',
        'catalog_category_product_index' => 'product_id',

        // for extension MageStore :: GiftVoucher
        'giftvoucher_product' => array(
            'keys' => 'product_id',
            'join' => array(
                'giftvoucher_history' => 'giftcard_product_id:giftvoucher_id'
            )
        )

    );

    protected $exportTablesM2 = array(
        "catalog_url_rewrite_product_category" => 'product_id',

        "catalog_product_entity_media_gallery_value" => array(
            'keys' => 'entity_id',
            'join' => array(
                'catalog_product_entity_media_gallery' => 'value_id:value_id',
                'catalog_product_entity_media_gallery_value_video' => 'value_id:value_id'
            )
        ),

        "catalog_product_entity_media_gallery" => array(
            'removeFromM1' => true
        ),

        "catalog_product_entity_media_gallery_value_to_entity" => 'entity_id',
    );
    protected $outputFile = self::DEFAULT_OUTPUT_FILE;
    protected $sqlInsetMode = self::DEFAULT_INSERT_MODE;
    protected $magentoVersion = self::DEFAULT_MAGENTO_VER;
    protected $useUniqueOrPk = self::DEFAULT_WITH_PK_UK;
    protected $credentialsDB = array();

    protected $pdo = null;


    protected $productIdsToExport = array();

    public function __construct(
        $magentoVersion,
        $outputFile,
        $sqlInsertMode,
        $useUIdx,
        $credentials
    )
    {
        $this->magentoVersion = $magentoVersion;
        $this->outputFile = $outputFile;
        $this->sqlInsetMode = $sqlInsertMode;
        $this->useUniqueOrPk = $useUIdx;
        $this->credentialsDB = $credentials;

        $this->establishDBConnection();
    }

    protected function establishDBConnection()
    {
        list($host, $user, $pwd, $db) = $this->credentialsDB;
        $dsn = "mysql:host=$host;dbname=$db;charset=utf8";
        $options = array(
            \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
        );
        try {
            $this->pdo = new \PDO($dsn, $user, $pwd, $options);
            $this->pdo->setAttribute(\PDO::ATTR_STATEMENT_CLASS, array("EPDOStatement\EPDOStatement", array($this->pdo)));
        } catch (\PDOException $exception) {
            exit(sprintf(
                'Error occurred during connecting to DB: ',
                $exception->getMessage()
            ));
        }
    }

    protected function getMagentoSpecificArray($version)
    {
        $resultMap = $this->exportTablesM1;
        if ('m2' == $version) {
            foreach ($this->exportTablesM2 as $tableName => $tableData) {
                if (is_array($tableData)) {
                    if (isset($tableData['removeFromM1']) && $tableData['removeFromM1']) {
                        unset($resultMap[$tableName]);
                    } else {
                        $resultMap = array_replace($resultMap, array($tableName => $tableData));
                    }
                } else {
                    $resultMap = array_replace($resultMap, array($tableName => $tableData));
                }
            }
        }
        return $resultMap;
    }

    protected function performQuery($sqlBlank, $params)
    {
        $sqlQuery = vsprintf($sqlBlank, $params);
        try {
            $stmt = $this->pdo->query($sqlQuery);
            $stmt->execute();
        } catch (\PDOException $exception) {
            die(sprintf(
                'Error occurred while executing next SQL script "%s", with error (%s): %s',
                $sqlQuery,
                $exception->getCode(),
                $exception->getMessage()
            ));
        } finally {
            return $stmt->fetchAll(\PDO::FETCH_UNIQUE);
        }
    }

    protected function prepareOrStatement($mapKeyParams, $ids)
    {
        if (!is_array($mapKeyParams)) {
            $mapKeyParams = array($mapKeyParams);
        } elseif (isset($mapKeyParams['keys'])) {
            $mapKeyParams = is_array($mapKeyParams['keys']) ? $mapKeyParams['keys'] : array($mapKeyParams['keys']);
        }
        $buffer = array();
        foreach ($mapKeyParams as $mapKeyParam) {
            $buffer[] = sprintf(
                '`%s` IN (' . implode(',', $ids) . ')',
                $mapKeyParam
            );
        }
        return implode(' OR ', $buffer);
    }

    protected function getPkForTable($table)
    {
        $sqlBlank = 'SHOW COLUMNS FROM `%s` WHERE `KEY` = "PRI"';
        $buffer = $this->performQuery($sqlBlank, $table);
        if ($buffer) {
            return key($buffer);
        } else {
            $sqlBlank = 'SHOW KEYS FROM `%s`;';
            if ($buffer = $this->performQuery($sqlBlank, $table)) {
                try {
                    return $buffer[$table]['Column_name'];
                } catch (Exception $exception) {
                    return false;
                }
            }
            return false;
        }
    }

    protected function getProductIdFromFetched($dataRow, $columnKey, $pKey, $rowId)
    {
        if (!is_array($columnKey)) {
            $foundKey = $columnKey;
            $columnKey = array($columnKey);
        } else {
            if (isset($columnKey['keys'])) {
                $columnKey = is_array($columnKey['keys']) ? $columnKey['keys'] : array($columnKey['keys']);
                $foundKey = $columnKey[0];
            } else {
                $columnKey = '';
                $foundKey = $columnKey;
            }
        }
        if (array_reduce(
            $columnKey,
            function($foundFlag, $item) use ($dataRow, &$foundKey) {
                $foundFlag = !$foundFlag ? isset($dataRow[$item]) : $foundFlag;
                if ($foundFlag) {
                    $foundKey = $item;
                }
                return $foundFlag;
            },
            false) && strlen($foundKey) && isset($dataRow[$foundKey])) {
            return $dataRow[$foundKey];
        } elseif (isset($dataRow[$pKey])) {
            return $dataRow[$pKey];
        } else {
            return $rowId;
        }
    }

    protected function appendProductsIdsFromFetched(&$data, $columnKey, $pKey, $map = array())
    {
        foreach ($data as $rowId => &$rowData) {
            $id = $this->getProductIdFromFetched(
                $rowData,
                $columnKey,
                $pKey,
                $rowId
            );
            if (1 < count($map)){
                isset($map[$id]) ? $rowData['.*pId*'] = $map[$id] : null;
            } else {
                $rowData['.*pId*'] = $id;
            }
        }
    }

    protected function getExistsTables()
    {
        $sqlQuery = 'SHOW TABLES;';
        $tables = $this->performQuery($sqlQuery, array());
        if ($tables) {
            return $tables;
        }
        return array();
    }

    protected function getDataAboutProducts($ids, $mode = 'SELECT')
    {
        $data = array();
        $map = $this->getMagentoSpecificArray($this->magentoVersion);
        $sqlBlank = '%s * FROM `%s` WHERE %s';

        $tables = $this->getExistsTables();

        foreach ($map as $table => $productIdColumn) {
            if (!isset($tables[$table])) {
                continue;
            }
            $params = array(
                $mode,
                $table,
                $this->prepareOrStatement($productIdColumn, $ids)
            );

            $buffer = $this->performQuery($sqlBlank, $params);

            if ($buffer) {
                $data[$table]['.*key*'] = $this->getPkForTable($table);
                $this->appendProductsIdsFromFetched($buffer, $productIdColumn, $data[$table]['.*key*']);
                $data[$table] += $buffer;

                if (is_array($productIdColumn) && isset($productIdColumn['join'])) {
                    foreach ($productIdColumn['join'] as $joinTableName => $joinTableField) {
                        $parts = explode(':', $joinTableField);
                        $mainTableField = $parts[0];
                        $joinTableField = 1 < count($parts) ? $parts[1] : $parts[0];

                        $joinTableByIds = array();
                        array_map(function ($item, $key) use ($mainTableField, &$joinTableByIds) {
                            $joinCId = isset($item[$mainTableField]) ? $item[$mainTableField] : $key;
                            $joinTableByIds[$joinCId] = $item['.*pId*'];
                        }, $buffer, array_keys($buffer));

                        $subParams = array(
                            $mode,
                            $joinTableName,
                            $this->prepareOrStatement($joinTableField, array_keys($joinTableByIds))
                        );

                        $subBuffer = $this->performQuery($sqlBlank, $subParams);
                        if ($subBuffer) {
                            $data[$joinTableName]['.*key*'] = $this->getPkForTable($joinTableName);
                            $this->appendProductsIdsFromFetched($subBuffer, $joinTableField, $data[$table]['.*key*'], $joinTableByIds);
                            $data[$joinTableName] += $subBuffer;
                        }
                    }
                }
            }
        }
        return $data;
    }

    protected function getKeyFieldsFrom($map, $tableName, $fieldsKeys, $addKeys = array())
    {
        $fields = is_array($map) ?
            $map[$tableName]
            : !is_null($fieldsKeys) ? $fieldsKeys : null;
        if (!$fields) {
            return array();
        }
        if (!is_array($fields)) {
            $fields = array($fields);
        }
        if (isset($fields['keys'])) {
            $result = is_array($fields['keys']) ? $fields['keys'] : array($fields['keys']);
        } else {
            $result = $fields;
        }
        $result = array_unique(array_merge($result, $addKeys));
        return $result;
    }

    protected function replaceIdsInFields(array &$fields, $idFields, $fromId, $toId, $addKeys = array())
    {
        foreach ($fields as $fieldName => &$fieldValue) {
            foreach ($this->getKeyFieldsFrom(null, null, $idFields, $addKeys) as $fieldNameToLookAt) {
                if ($fieldNameToLookAt == $fieldName && $fieldValue == $fromId) {
                    $fieldValue = $toId;
                }
            }
        }
    }

    protected function hookAddSlashesIfSlashes(array &$data)
    {
        foreach ($data as &$item) {
            if (false !== strpos($item, '\\')) {
                $item = preg_replace('/(\\\[\w]+)/', '\\\$1', $item);
            }
        }
    }

    protected function hookOnFieldFromMap(&$map, $tableName, $cache = true)
    {
        if (!isset($map[$tableName])) {
            $result = array();
            foreach ($map as $item => $itemData) {
                if (is_array($itemData) && isset($itemData['join'])) {
                    foreach ($itemData['join'] as $joinTableName => $joinTableField) {
                        if ($tableName == $joinTableName) {
                            $parts = explode(':', $joinTableField);
                            $result = array(1 < $parts ? $parts[1] : $parts[0]);
                            if ($cache) {
                                $map[$tableName] = $result;
                            }
                            return $result;
                        }
                    }
                }
            }
        } else {
            return $map[$tableName];
        }
        return $result;
    }

    protected function generateSql(array $data, array $ids)
    {
        $sql = array();
        $insertMode = $this->sqlInsetMode;
        $map = $this->getMagentoSpecificArray($this->magentoVersion);
        $insertPk = $this->useUniqueOrPk;

        foreach ($data as $tableName => $tableData) {
            $pKey = $tableData['.*key*'];
            unset($tableData['.*key*']);
            if (1 > count($tableData)) {
                continue;
            }
            foreach ($tableData as $productId => $productFields) {
                $pId = $productFields['.*pId*'];
                unset($productFields['.*pId*']);
                $columns = array_keys($productFields);
                if (1 > count($columns)) {
                    continue;
                }

                if (!isset($sql[$pId])) {
                    $sql[$pId] = array();
                }

                if ($insertPk && !isset($columns[$pKey])) {
                    $columns[] = $pKey;
                    $productFields[$pKey] = $productId;
                }

                if (isset($ids[$pId])) {
                    $this->replaceIdsInFields(
                        $productFields,
                        $this->hookOnFieldFromMap($map, $tableName),
                        $pId,
                        $ids[$pId],
                        array($pKey)
                    );
                }

                $sqlQuery = sprintf(
                    "%s INTO `%s` \r\t(`"
                    . implode('`,`', $columns) . '`)'
                    . "\r\t VALUES ("
                    . implode(',', array_fill(1, count($columns), '?'))
                    . ');',
                    $insertMode,
                    $tableName
                );

                $this->hookAddSlashesIfSlashes($productFields);

                /**
                 * @var $stmt EPDOStatement
                 */
                $stmt = $this->pdo->prepare($sqlQuery);
                $sql[$pId][] = sprintf(
                    "\r/* data from table %s, id = %s, productId = %s */\r%s",
                    $tableName,
                    $productId,
                    $pId,
                    $stmt->interpolateQuery(array_values($productFields))
                );
            }
        }
        return $sql;
    }

    protected function concatenateSqlScript($data)
    {
        $result = array();
        $result[] = sprintf(
            "/*\rEXPORT OF PRODUCTS FROM \rDB: %s\rHOST: %s\rMagento Version: %s\rProducts' Ids: %s (total count: %d)\r*/\r",
            $this->credentialsDB[3],
            $this->credentialsDB[0],
            $this->magentoVersion,
            implode(', ', array_keys($data)),
            count($data)
        );
        $result[] = $this->epilogueDisableChecks['before'];
        foreach ($data as $productId => $productSql) {
            $result[] = sprintf("\r" . '/* BEGIN TRANSACTION FOR PRODUCT ID: %s */', $productId);
            $result[] = 'START TRANSACTION;';
            foreach ($productSql as $sql) {
                $result[] = $sql;
            }
            $result[] = 'COMMIT;';
            $result[] = sprintf("\r" . '/* END TRANSACTION FOR PRODUCT ID: %s */', $productId);
        }
        $result[] = $this->epilogueDisableChecks['after'];
        return implode("\r", $result);
    }

    public function exportProducts($ids)
    {
        $data = $this->getDataAboutProducts($ids, 'SELECT');
        $sql = $this->generateSql($data, $ids);
        file_put_contents($this->getFileName(), $this->concatenateSqlScript($sql));
    }

    public function getFileName()
    {
        $prefix = 0;
        $ext = pathinfo($this->outputFile, PATHINFO_EXTENSION);
        $path = pathinfo($this->outputFile, PATHINFO_DIRNAME);
        $base = pathinfo($this->outputFile, PATHINFO_FILENAME);
        $filePath = $path . DIRECTORY_SEPARATOR . $base . '.' . $ext;

        while (file_exists($filePath)) {
            $filePath = $path . DIRECTORY_SEPARATOR . $base . '-' . sprintf('%\'.03d', $prefix++) . '.' . $ext;
        }

        return $this->outputFile = $filePath;
    }
}

if (!file_exists('credentials.php')) {
    exit('File with DB credentials doesn\'t exists');
}

$credentials = require('credentials.php');

$ids = array();
if (isset($argv[1])) {
    array_map(function($item) use(&$ids) {
        $parts = explode(':', trim($item));
        $id = $parts[0];
        if (1 < count($parts)) {
            $toId = $parts[1];
        } else {
            $toId = $id;
        }
        if (!is_numeric($id)) {
            return null;
        }
        $ids[$id] = $toId;
        return array($id => $toId);
    }, explode(',', $argv[1]));
}
$version = isset($argv[2]) ? $argv[2] : ProductExport2Sql::DEFAULT_MAGENTO_VER;
$outputFile = isset($argv[3]) ? $argv[3] : ProductExport2Sql::DEFAULT_OUTPUT_FILE;
$sqlMode = isset($argv[4]) ? $argv[4] : ProductExport2Sql::DEFAULT_INSERT_MODE;
$useUIdx = isset($argv[5]) ? $argv[5] : ProductExport2Sql::DEFAULT_WITH_PK_UK;

$exporter = new ProductExport2Sql($version, $outputFile, $sqlMode, $useUIdx, $credentials);
$exporter->exportProducts($ids);