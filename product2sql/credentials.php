<?php
    $credentialsAliased = array(
        'host' => 'localhost',
        'user' => 'root',
        'password' => 'root',
        'db' => 'thinkliquor_local_16'
    );

    // in order to keep right order
    return array(
        $credentialsAliased['host'],
        $credentialsAliased['user'],
        $credentialsAliased['password'],
        $credentialsAliased['db']
    );
