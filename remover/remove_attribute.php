<?php

error_reporting(E_ALL | E_STRICT);

require_once './app/Mage.php';
umask(0);
Mage::app();

$attributesArray = array('mailchimp_sync_delta', 'mailchimp_sync_error', 'mailchimp_sync_modified'); //attribute code to remove

$setup = Mage::getResourceModel('catalog/setup', 'core_setup');
try {
    $setup->startSetup();
    foreach ($attributesArray as $attribute) {
        try {
            $setup->removeAttribute('catalog_product', $attribute);
        } catch (Exception $exception) {
            print_r($exception->getMessage());
        } finally {
            echo $attribute . ' attribute is removed' . PHP_EOL;
        }
    }
    $setup->endSetup();

} catch (Mage_Core_Exception $e) {
    print_r($e->getMessage());
}
