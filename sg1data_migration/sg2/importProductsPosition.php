<?php

require 'EPDOStatement.php';

error_reporting(E_ALL | E_STRICT);

class ImportProductsPosition
{
    protected $_mageUser = 'admin';
    protected $_logFile = 'import_order_log_';
    protected $_extraBuffer = array();
    /** @var  \PDO */
    protected $pdo;

    public function __construct($logFile)
    {
        $this->_basePath = getcwd();
        $this->_logFile = $logFile . '-' . $this->_logFile . date('Y-m-d_H-i-s') . '.log';

        $this->initMagento();
        $this->launchAdmin();
    }

    public function initMagento()
    {
        define('MAGENTO_ROOT', getcwd());
        require_once MAGENTO_ROOT . '/app/Mage.php';
        umask(0);

        Mage::app();
    }

    public function launchAdmin()
    {
        Mage::getSingleton('core/session', array('name' => 'adminhtml'));

        $request = Mage::app()->getRequest();
        $response = Mage::app()->getResponse();

        $user = Mage::getModel('admin/user')->loadByUsername($this->_mageUser);
        if (Mage::getSingleton('adminhtml/url')->useSecretKey()) {
            Mage::getSingleton('adminhtml/url')->renewSecretUrls();
        }
        $session = Mage::getSingleton('admin/session');
        $session->setIsFirstVisit(true);
        $session->setUser($user);
        $session->setAcl(Mage::getResourceModel('admin/acl')->loadAcl());
        Mage::dispatchEvent('admin_session_user_login_success', array('user' => $user));
    }

    // 1. Read Categories mapping
    // 2. Map products from 2 DB (SKU--ID)
    // 3. Generate SQL



    public function readCategoriesMapping($file)
    {
        // NAME | ID Correct | ID Wrong
        $result = false;
        if ($file && file_exists($file)) {
            $result = array();
            $fh = fopen($file, 'r');
            // read header
            $buffer = fgetcsv($fh);
            while (!feof($fh)) {
                $buffer = fgetcsv($fh);
                if (!is_null($buffer[1]) && !is_null($buffer[2]) && strlen($buffer[1]) && strlen($buffer[2])) {
                    // If now column -- error. | [in new db => in old db]
                    $result[$buffer[2]] = $buffer[1];
                }
            }
            fclose($fh);
        }
        return $result;
    }

    public function readOuterProductsPosition($file)
    {
        $result = false;
        if ($file && file_exists($file)) {
            $result = array();
            $fh = fopen($file, 'r');
            while (!feof($fh)) {
                $buffer = fgetcsv($fh);
                if (!is_null($buffer[0]) && !is_null($buffer[1])) {
                    // If now column -- error.
                    $result[$buffer[0]] = $buffer[1];
                }
            }
            fclose($fh);
        }
        return $result;
    }

    public function loadInnerProducts()
    {
        $result = array();
        /** @var Mage_Catalog_Model_Resource_Product_Collection  $ordersCollection */
        $ordersCollection = Mage::getModel('catalog/product')->getCollection();
        $items = $ordersCollection->load()->getItems();

        foreach ($items as $item) {
            $result[$item->getSku()] = $item->getId();
        }

        return $result;
    }

    public function connectToDb($container)
    {
        $user = $container->getUser();
        $password = $container->getPassword();
        $db = $container->getDb();
        $host = $container->getHost();
        $dsn = "mysql:host=$host;dbname=$db;charset=utf8";
        $options = array(
            \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
        );
        try {
            $this->pdo = new \PDO($dsn, $user, $password, $options);
            $this->pdo->setAttribute(\PDO::ATTR_STATEMENT_CLASS, array("EPDOStatement\EPDOStatement", array($this->pdo)));
        } catch (\PDOException $exception) {
            exit(sprintf(
                'Error occurred during connecting to DB: ',
                $exception->getMessage()
            ));
        }
    }

    public function collectPositionIndexFromOuterDB()
    {
        $query = 'SELECT * FROM catalog_category_product;';
        $stmt = $this->pdo->query($query);
        $result = $stmt->fetchAll();
        return $result;
    }

    public function generateProductsMap($outerProductsIds, $innerProductsIds)
    {
        $result = array();
        foreach ($innerProductsIds as $innerSku => $innerId) {
            if (isset($outerProductsIds[$innerSku])) {
                $outerId = $outerProductsIds[$innerSku];
                $result[$outerId] = $innerId;
            }
        }
        return $result;
    }

    public function processPositionsIndex($positions, $productsMap, $categoriesMap)
    {
        $result = array();
        foreach ($positions as $index => $positionData) {
            $catIdOrigin = $positionData['category_id'];
            $productId = $positionData['product_id'];
            if (
                isset($categoriesMap[$catIdOrigin]) &&
                isset($productsMap[$productId]) // &&
                // 1 < (int)$positionData['position']
            ) {
                $positionData['category_id'] = (int)$categoriesMap[$catIdOrigin];
                $positionData['product_id'] = (int)$productsMap[$productId];
                $positionData['position'] = (int)$positionData['position'];
                $result[] = $positionData;
            }
        }
        return $result;
    }

    public function generateSql($array)
    {
        $sql = array();
        $format = 'REPLACE INTO `catalog_category_product` (`%s`, `%s`, `%s`) VALUES (:category_id, :product_id, :position);';
        $sqlQuery = vsprintf($format, array('category_id', 'product_id', 'position'));
        foreach ($array as $index => $item) {
            $stmt = $this->pdo->prepare($sqlQuery);
            $buffer = $stmt->interpolateQuery($item);
            $sql[] = $buffer;
        }
        return implode(PHP_EOL, $sql);
    }

    public function importToDb($sql)
    {
        $installer = new Mage_Core_Model_Resource_Setup('core_setup');
        $installer->run($sql);
    }

    public function runProductsImport($fileCategoriesCsv, $fileOuterProductsCsv, $container)
    {
        $categoriesMap = $this->readCategoriesMapping($fileCategoriesCsv);
        $outerProductsIds = $this->readOuterProductsPosition($fileOuterProductsCsv);
        $innerProductsIds = $this->loadInnerProducts();
        $productsMap = $this->generateProductsMap($outerProductsIds, $innerProductsIds);
        $this->connectToDb($container);
        $outerPositions = $this->collectPositionIndexFromOuterDB();
        $dataToPush = $this->processPositionsIndex($outerPositions, $productsMap, $categoriesMap);
        $sql = $this->generateSql($dataToPush);
        $this->importToDb($sql);
    }
}

$fileOuterProductsCsv = '/var/www/spiritedgifts.loc/public_html/export.products.ids.22-07-2017_01-06-13.csv';
$fileCategoriesCsv = '/var/www/spiritedgifts2.loc/public_html/category_ids.csv';

$importer = new ImportProductsPosition('import.products.position.log');
$container = new Varien_Object();
$container->setDb('spiritedgifts_local_5');
$container->setUser('root');
$container->setPassword('root');
$importer->runProductsImport($fileCategoriesCsv, $fileOuterProductsCsv, $container);