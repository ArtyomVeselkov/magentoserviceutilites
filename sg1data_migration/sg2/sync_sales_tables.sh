db_host=$1
db_name=$2
db_user=$3
db_password=$4
db_host_to=$5
db_name_to=$6
db_user_to=$7
db_password_to=$8
tables_exclude=$9
sql_patch_before=${10}
sql_patch_after=${11}

if [ -f "${sql_patch_before}" ]; then
mysql -h$db_host_to -u$db_user_to -p"${db_password_to}" $db_name_to < "${10}";
fi

while read in; do pt-table-sync --verbose --no-foreign-key-checks --no-check-slave --execute "h=${db_host},D=${db_name},u=${db_user},p=${db_password},t=${in}" "h=${db_host_to},D=${db_name_to},u=${db_user_to},p=${db_password_to}"; done < $9

if [ -f "${sql_patch_after}" ]; then
mysql -h$db_host_to -u$db_user_to -p"${db_password_to}" $db_name_to < "${11}";
fi