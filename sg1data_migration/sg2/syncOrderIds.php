<?php

error_reporting(E_ALL | E_STRICT);

class syncOrderIds
{
    protected $_basePath;
    protected $_logFile;
    protected $_mageUser;
    protected $_storeConnection;
    protected $_storeConnectionSession;
    protected $_splitCallLimit = 10;
    public $file;
    public $connectionContainer;

    public function __construct($magentoUser, $logFile)
    {
        $this->_basePath = getcwd();
        $this->_logFile = $logFile . '-' . $this->_logFile . date('Y-m-d_H-i-s') . '.log';

        $this->initMagento();
        $this->launchAdmin();
    }

    public function initMagento()
    {
        define('MAGENTO_ROOT', getcwd());
        require_once MAGENTO_ROOT . '/app/Mage.php';
        umask(0);

        Mage::app();
    }

    public function launchAdmin()
    {
        Mage::getSingleton('core/session', array('name' => 'adminhtml'));

        $request = Mage::app()->getRequest();
        $response = Mage::app()->getResponse();
        require_once 'app/code/community/Raveinfosys/Exporter/controllers/Adminhtml/ImporterController.php';
        $controller = new Mage_Adminhtml_Controller_Action($request, $response);

        $user = Mage::getModel('admin/user')->loadByUsername($this->_mageUser);
        if (Mage::getSingleton('adminhtml/url')->useSecretKey()) {
            Mage::getSingleton('adminhtml/url')->renewSecretUrls();
        }
        $session = Mage::getSingleton('admin/session');
        $session->setIsFirstVisit(true);
        $session->setUser($user);
        $session->setAcl(Mage::getResourceModel('admin/acl')->loadAcl());
        Mage::dispatchEvent('admin_session_user_login_success', array('user' => $user));
    }

    public function array_combine_($keys, $values)
    {
        $result = array();
        foreach ($keys as $i => $k) {
            $result[$k][] = $values[$i];
        }
        array_walk($result, create_function('&$v', '$v = (count($v) == 1)? array_pop($v): $v;'));
        return    $result;
    }


    public function log($section, $string, $echoFlag = true)
    {
        $line = sprintf(
            '[%s] %s :: %s' . PHP_EOL,
            date('d.m.Y H:i:s'),
            $section,
            $string
        );
        $content = file_get_contents($this->_logFile);
        $content = $content . PHP_EOL . $line;
        file_put_contents($this->_logFile, $content);
        if ($echoFlag) {
            echo $line;
        }
    }

    /**
     * @param Varien_Object $container
     */
    public function connectToStore($container)
    {
        $url = $container->getUrl();
        $user = $container->getUser();
        $password = $container->getPassword();
        $params = $container->getParams();
        $cli = new SoapClient($url, $params);
        if ($cli) {
            $this->_storeConnection = $cli;
        } else {
            $this->log(__FUNCTION__, 'Unable establish SOAP connection to store.');
            return ;
        }
        try {
            $param = (object)array('username' => $user, 'apiKey' => $password);
            $this->_storeConnectionSession = $this->_storeConnection->login($param);
        } catch (Exception $exception) {
            $this->log(__FUNCTION__, $exception->getMessage());
        }
        $this->_storeConnection;
    }

    public function getDefaultContainer()
    {
        $container = new Varien_Object();
        $container->setData('url', 'https://www.site.com/index.php/api/soap/?wsdl=1');
        $container->setData('user', 'user');
        $container->setData('password', 'password');
        $params =  array(
            'trace' => 1,
            'exceptions' => true,
            'cache_wsdl' => WSDL_CACHE_NONE,
        );
        $container->setData('params', $params);
    }

    public function callOrderList($idFrom, $idTo)
    {
        $result = false;
        $params = array();
        $params['complex_filter'] = array(
            array(
                'key' => 'order_id',
                'value' => array(
                    array(
                        'key' => 'gteq',
                        'value' => $idFrom
                    ),
                    array(
                        'key' => 'lt',
                        'value' => $idTo
                    )
                )
            )
        );
        try {
            $requestObject = (object)array(
                'sessionId' => $this->_storeConnectionSession->result,
                'filters' => $params
            );
            $result = $this->_storeConnection->salesOrderList($requestObject);
            $result = $result->result->complexObjectArray;
        } catch (Exception $exception) {
            $this->log(__FUNCTION__, $exception->getMessage());
        }
        return $result;
    }

    public function getOuterOrdersList($idFrom = 30000)
    {
        $result = array();
        $count = $idFrom / $this->_splitCallLimit + 1;
        $counter = 0;
        while ($counter <= $count) {
            $from = $counter * $this->_splitCallLimit;
            $to = $from + $this->_splitCallLimit;
            if ($from > $idFrom && $to > $idFrom) {
                break;
            } elseif ($from < $idFrom && $to > $idFrom) {
                $to = $idFrom;
            }
            $counter++;
            $buffer = $this->callOrderList($from, $to);
            if (is_array($buffer)) {
                array_push($result, $buffer);
            }
        }
        return $result;
    }

    public function processOuterOrders($resultSoapResult)
    {
        $result = array();
        if ($resultSoapResult) {
            $array = $resultSoapResult->result->complexObjectArray;
        } else {
            return ;
        }
        foreach ($array as $item) {
            $inc = $item->increment_id;
            $id = $item->order_id;
            $result[$inc] = $id;
        }
    }

    public function getCurrentStoreOrders()
    {
        $result = array();
        /** @var Mage_Sales_Model_Resource_Order_Collection  $ordersCollection */
        $ordersCollection = Mage::getModel('sales/order')->getCollection();
        $ordersCollection->addFieldToSelect('entity_id');
        $ordersCollection->addFieldToSelect('increment_id');
        $result = $ordersCollection->load()->getItems();
        return $result;
    }

    public function getIdsForInnerOrdersList($orders)
    {
        $result = array();
        foreach ($orders as $order) {
            /** @var Mage_Sales_Model_Order $order */
            $inc = $order->getIncrementId();
            $id = $order->getId();
            $result[$inc] = $id;
        }
        return $result;
    }

    public function getIdsForOuterOrdersList($outerList)
    {
        $result = array();
        foreach ($outerList as $index => $orderData) {
            if (isset($orderData['increment_id']) && isset($orderData['order_id'])) {
                $inc = $orderData['increment_id'];
                $id = $orderData['order_id'];
                $result[$inc] = $id;
            }
        }
        return $result;
    }

    public function getOuterOrdersListForFile()
    {
        // (!) This variant is probably not acceptable as in CSV there is not info about entity_id
        $csv = array();
        if (file_exists($this->file)) {
            $fileHandle = fopen($this->file, 'r');
            $headers = fgetcsv($fileHandle);
            while (!feof($fileHandle)) {
                $csv[] = fgetcsv($fileHandle);
                if (!is_array($csv) || !count($csv)) {
                    return false;
                }
                $csv = array();
            }
            $idColimnIndex = array_search('order_id', $headers);
        }
        // get columns index for id and inc_id
        // then build list
        // then return
        return $csv;
    }

    /**
     * ['id old' => 'id new (from live store)']
     *
     * @return array
     */
    public function getMapping()
    {
        return array();
    }

    public function diffIds(&$targetIds, $outerIds = null)
    {
        $innerOrders = $this->getCurrentStoreOrders();
        $innerIds = $this->getIdsForInnerOrdersList($innerOrders);

        if (is_null($outerIds)) {
            $outerOrders = $this->getOuterOrdersList();
            $outerIds = $this->getIdsForOuterOrdersList($outerOrders);
        }
        $targetIds = $outerIds;
        $diff = array_diff_assoc($innerIds, $outerIds);
        arsort($diff);
        return $diff;
    }

    public function getMappingFromDiff($diff)
    {
        // here logic to get mapping
        return $this->getMapping($diff);
    }

    public function generateSql($idOld, $idNew, $incrementId)
    {
        $sql = <<<SQL
UPDATE `sales_flat_order` SET `entity_id` = $idNew  WHERE `entity_id` = $idOld AND `increment_id` = '$incrementId';
SQL;
        return $sql;
    }

    public function createScriptsForReplacements($mapping, $targetIds)
    {
        if (!is_array($mapping)) {
            return  false;
        }
        $result = array();
        $stack = array();
        foreach ($mapping as $increment => $idTarget) {
            if (!isset($targetIds[$increment])) {
                continue;
                // delta
            }
            $idNeedle = $targetIds[$increment];
            $stack[$idNeedle] = array(
                'id2r' => $idTarget,
                'inc' => $increment
            );
        }
        // We should rsort based on new IDs, but as there could be situation of 1,2,3 ==> 3,2,1, so we disable FK check and replace by entity_id and increment
        krsort($stack);
        foreach ($stack as $idNeedle => $item) {
            $buffer = $this->generateSql($item['id2r'], $idNeedle, $item['inc']);
            if ($buffer) {
                $result[] = $buffer;
            }
        }
        return $result;
    }

    public function loadCsvPair($file)
    {
        $result = false;
        if ($file && file_exists($file)) {
            $result = array();
            $fh = fopen($file, 'r');
            while (!feof($fh)) {
                $buffer = fgetcsv($fh);
                if (!is_null($buffer[0]) && !is_null($buffer[1])) {
                    // If now column -- error.
                    $result[$buffer[0]] = $buffer[1];
                }
            }
            fclose($fh);
        }
        return $result;
    }

    public function runCompareIds($innerIdsFile = null)
    {
        $innerIds = $this->loadCsvPair($innerIdsFile);
        if (!is_array($innerIds) || !count($innerIds)) {
            $innerIds = null;
        }
        $diff = $this->diffIds($targetIds, $innerIds);
        $this->processDeltaDiff($diff, $targetIds);
        // ksort($diff);
        // $mapping = $this->getMappingFromDiff($diff);
        // FIRST we need to replace all ids to bigger one to prevent duplication errors (as column has unique key).
        $startFrom = (int)$this->getAiValue('sales_flat_order') + 1;
        $remapper = array();
        $targetIdsBigger = $this->getTransitBiggerIds($targetIds, $startFrom, $remapper);
        $scripts = $this->createScriptsForReplacements($diff, $targetIdsBigger);
        $sql = implode(PHP_EOL, $scripts);
        // SECOND we replace ids to their actual target values.
        $scripts = $this->createScriptsForReplacements($targetIdsBigger, $targetIds);
        $sql = $sql . PHP_EOL . '/* ************ ACTUAL VALUES ************ */' . PHP_EOL . implode(PHP_EOL, $scripts);
        $this->runQueries($sql);
    }

    public function processDeltaDiff($diff, &$targetIds)
    {
        $maxTargetId = max($targetIds);
        /*
           $maxTargetId = array_reduce($targetIds, function ($value, &$buffer) {
            if ($value > $buffer) {
                $buffer = $value;
            }
        });
        */
        foreach ($diff as $increment => $currentId) {
            if (!isset($targetIds[$increment])) {
                $maxTargetId++;
                $targetIds[$increment] = $maxTargetId;
            }
        }
    }

    public function generateRevertTargetIds($remapper, $targetIds)
    {
        $result = array();
        foreach ($remapper as $increment => $item) {
            $originId = $remapper['origin'];
        }
    }

    public function getAiValue($table)
    {
        $sql = <<<SQL
SELECT `AUTO_INCREMENT`
FROM  INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = DATABASE()
AND   TABLE_NAME   = '$table';
SQL;
        $installer = new Mage_Core_Model_Resource_Setup('core_setup');
        $result = $installer->getConnection()->fetchOne($sql);
        return $result;
    }

    public function getTransitBiggerIds($array, $startFrom, &$remapper = array())
    {
        $counter = 0;
        $result = array();
        $remapper = array();
        foreach ($array as $increment => $id) {
            $newId = $startFrom + $counter;
            $remapper[$increment] = array(
                'origin' => $id,
                'replaced' => $newId
            );
            $result[$increment] = $newId;
            $counter++;
        }
        return $result;
    }

    public function runQueries($queries)
    {
        $installer = new Mage_Core_Model_Resource_Setup('core_setup');
        // $installer->startSetup();
        $queries = '
/*
SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT;
SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS;
SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION;
SET NAMES utf8;
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE=\'NO_AUTO_VALUE_ON_ZERO\';
SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0;
*/
START TRANSACTION; 
' . $queries;
        $queries = $queries .
' COMMIT; 
/*
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT;
SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS;
SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION;
SET SQL_NOTES=@OLD_SQL_NOTES;
*/
';
        $installer->run($queries);
        // $installer->endSetup();
    }

    public function getFkTablesOrderId()
    {
        return array(
            'amasty_order_flag' => 'order_id',
            'sales_flat_invoice_grid' => 'order_id',
            'sales_flat_creditmemo_grid' => 'order_id',
            'sales_flat_shipment_grid' => 'order_id',
            'sales_flat_shipment_track' => 'order_id',
            'sales_order_tax' => 'order_id'
        );
    }

    public function alterFkOrderId()
    {
        $tables = $this->getFkTablesOrderId();
        $this->alterAddFkOrderId($tables);
    }

    public function alterAddFkOrderId($tables)
    {
        foreach ($tables as $table => $column) {
            $this->processAddFkToTableOrderId($table, $column);
        }
    }

    public function processAddFkToTableOrderId($table, $column)
    {
        $installer = new Mage_Core_Model_Resource_Setup('core_setup');
        $installer->startSetup();
        $installer->getConnection()->addForeignKey(
            $installer->getFkName(
                $table,
                $column,
                'sales_flat_order',
                'entity_id'
            ),
            $table,
            $column,
            'sales_flat_order',
            'entity_id',
            Varien_Db_Ddl_Table::ACTION_NO_ACTION,
            Varien_Db_Ddl_Table::ACTION_CASCADE
        );
        $installer->endSetup();
    }
}

$sync = new syncOrderIds('admin', 'sync');

$innerIdsFile = '/var/www/spiritedgifts.loc/public_html/export.order.ids.22-07-2017_19-09-59.csv';

$container = new Varien_Object();
$container->setData('url', 'http://spiritedgifts.loc/index.php/api/v2_soap/?wsdl=1');
// $container->setData('url', 'https://www.spiritedgifts.com/index.php/api/v2_soap/?wsdl');
$container->setData('user', 'Klaviyo');
$container->setData('password', '!$!Qo30i0HH8!on');
$params =  array(
    'trace' => 1,
    'exceptions' => true,
    'cache_wsdl' => WSDL_CACHE_NONE,
);
$container->setData('params', $params);

$sync->connectionContainer = $container;
$sync->alterFkOrderId();
// $sync->connectToStore($sync->connectionContainer);
$sync->runCompareIds($innerIdsFile);

// $client->endSession($session);