<?php

require 'EPDOStatement.php';

error_reporting(E_ALL | E_STRICT);

class UpdateCustomerRefs
{
    protected $_mageUser = 'admin';
    protected $_logFile = 'import_order_log_';
    protected $_extraBuffer = array();
    /** @var  \PDO */
    protected $pdo;

    public function __construct($logFile)
    {
        $this->_basePath = getcwd();
        $this->_logFile = $logFile . '-' . $this->_logFile . date('Y-m-d_H-i-s') . '.log';

        $this->initMagento();
        $this->launchAdmin();
    }

    public function initMagento()
    {
        define('MAGENTO_ROOT', getcwd());
        require_once MAGENTO_ROOT . '/app/Mage.php';
        umask(0);

        Mage::app();
    }

    public function launchAdmin()
    {
        Mage::getSingleton('core/session', array('name' => 'adminhtml'));

        $request = Mage::app()->getRequest();
        $response = Mage::app()->getResponse();

        $user = Mage::getModel('admin/user')->loadByUsername($this->_mageUser);
        if (Mage::getSingleton('adminhtml/url')->useSecretKey()) {
            Mage::getSingleton('adminhtml/url')->renewSecretUrls();
        }
        $session = Mage::getSingleton('admin/session');
        $session->setIsFirstVisit(true);
        $session->setUser($user);
        $session->setAcl(Mage::getResourceModel('admin/acl')->loadAcl());
        Mage::dispatchEvent('admin_session_user_login_success', array('user' => $user));
    }

    // 1. Read Categories mapping
    // 2. Map products from 2 DB (SKU--ID)
    // 3. Generate SQL


    public function loadInnerCustomers()
    {
        $result = array();
        /** @var Mage_Customer_Model_Resource_Customer_Collection $customersCollection */
        $customersCollection = Mage::getModel('customer/customer')->getCollection();
        $items = $customersCollection->load()->getItems();

        foreach ($items as $item) {
            $result[$item->getEmail()] = $item->getId();
        }

        return $result;
    }

    public function connectToDb($container)
    {
        $user = $container->getUser();
        $password = $container->getPassword();
        $db = $container->getDb();
        $host = $container->getHost();
        $dsn = "mysql:host=$host;dbname=$db;charset=utf8";
        $options = array(
            \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
        );
        try {
            $this->pdo = new \PDO($dsn, $user, $password, $options);
            $this->pdo->setAttribute(\PDO::ATTR_STATEMENT_CLASS, array("EPDOStatement\EPDOStatement", array($this->pdo)));
        } catch (\PDOException $exception) {
            exit(sprintf(
                'Error occurred during connecting to DB: ',
                $exception->getMessage()
            ));
        }
    }

    public function collectCustomersIdsFromOuterDb()
    {
        $query = 'SELECT * FROM customer_entity;';
        $stmt = $this->pdo->query($query);
        $result = $stmt->fetchAll();
        $result = $this->reprocessEmails($result);
        return $result;
    }

    public function getAiValue($table)
    {
        $sql = <<<SQL
SELECT `AUTO_INCREMENT`
FROM  INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = DATABASE()
AND   TABLE_NAME   = '$table';
SQL;
        $installer = new Mage_Core_Model_Resource_Setup('core_setup');
        $result = $installer->getConnection()->fetchOne($sql);
        return $result;
    }

    public function generateCustomersMap($outerIds, $innerIds)
    {
        $result = array();
        $maxInnerId = (int)$this->getAiValue('customer_entity');
        $counter = $maxInnerId;
        foreach ($innerIds as $innerEmail => $innerId) {
            $counter++;
            if (isset($outerIds[$innerEmail])) {
                $outerId = $outerIds[$innerEmail];
                $result[] = array(
                    'customer_new_id' => $innerId,
                    'customer_transit_id' => $counter,
                    'customer_old_id' => $outerId
                );
            }
        }
        return $result;
    }

    public function reprocessEmails($array)
    {
        $result = array();
        foreach ($array as $item) {
            $email = $item['email'];
            $id = $item['entity_id'];
            $result[$email] = $id;
        }
        return $result;
    }

    public function generateSql($array, $table, $column, $valueWhere, $valueSet)
    {
        $sql = array();
        $format = 'UPDATE `%s` SET `%s` = :%s WHERE `%s` = :%s;';
        $sqlQuery = vsprintf($format, array($table, $column, $valueSet, $column, $valueWhere));
        foreach ($array as $index => $item) {
            $stmt = $this->pdo->prepare($sqlQuery);
            $buffer = $stmt->interpolateQuery($item);
            $sql[] = $buffer;
        }
        return implode(PHP_EOL, $sql);
    }

    public function generateSqlNullize($table, $column)
    {
        $sql = array();
        $format = 'UPDATE `%s` SET `%s` = NULL;';
        $sqlQuery = vsprintf($format, array($table, $column));
        $stmt = $this->pdo->prepare($sqlQuery);
        $buffer = $stmt->interpolateQuery(array());
        $sql[] = $buffer;
        return implode(PHP_EOL, $sql);
    }

    public function importToDb($sql)
    {
        $installer = new Mage_Core_Model_Resource_Setup('core_setup');
        $installer->startSetup();
        $installer->run($sql);
        $installer->endSetup();
    }

    public function importToDb2($sql)
    {
        $installer = new Mage_Core_Model_Resource_Setup('core_setup');
        $queries = '

SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT;
SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS;
SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION;
SET NAMES utf8;
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE=\'NO_AUTO_VALUE_ON_ZERO\';
SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0;

START TRANSACTION; 
' . $sql;
        $queries = $queries .
            ' COMMIT; 

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT;
SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS;
SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION;
SET SQL_NOTES=@OLD_SQL_NOTES;
';

        $installer->getConnection()->query($queries);
    }

    public function runCustomersUpdate($columnCustomerId, $columnsNull, $container)
    {
        $this->connectToDb($container);
        $outerIds = $this->collectCustomersIdsFromOuterDb();
        $innerIds = $this->loadInnerCustomers();
        $map = $this->generateCustomersMap($outerIds, $innerIds);
        $stack = array();
        // uniquize
        foreach ($columnCustomerId as $table => $column) {
            $sql = $this->generateSql($map, $table, $column, 'customer_old_id', 'customer_transit_id');
            $stack[] = $sql;
        }
        // restore
        foreach ($columnCustomerId as $table => $column) {
            $sql = $this->generateSql($map, $table, $column, 'customer_transit_id', 'customer_new_id');
            $stack[] = $sql;
        }
        // nullize
        foreach ($columnsNull as $table => $column) {
            $sql = $this->generateSqlNullize($table, $column);
            $stack[] = $sql;
        }
        $result = implode(PHP_EOL . ' ', $stack);
        $this->importToDb2($result);
    }

}

$importer = new UpdateCustomerRefs('update.customers.ids.log');
$container = new Varien_Object();
$container->setDb('spiritedgifts_local_6');
$container->setUser('root');
$container->setPassword('root');
$columnCustomerId = array(
    'sales_flat_order' => 'customer_id',
    'sales_flat_order_address' => 'customer_id',
    'sales_flat_order_grid' => 'customer_id',
    'sales_flat_quote' => 'customer_id',
    'sales_flat_quote_address' => 'customer_id',
    'sales_flat_shipment' => 'customer_id'
);
$columnsNull = array(
    'sales_flat_order_address' => 'customer_address_id',
    'sales_flat_quote_address' => 'customer_address_id'
);
echo 'START: ' . date('d.m.Y H:i:s') . PHP_EOL;
try {
    $importer->runCustomersUpdate($columnCustomerId, $columnsNull, $container);
} catch (Exception $exception) {
    var_dump($exception);
}
echo PHP_EOL . 'END: ' . date('d.m.Y H:i:s');