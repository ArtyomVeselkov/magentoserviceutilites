<?php

error_reporting(E_ALL | E_STRICT);

class CSVSplitter {
    public $_filePath;
    public $_fileHandle;
    public $_fileContent;
    public $_headers;
    public $_limit = 200;
    public $_saveDirectory = 'var/export/orders_import_tmp';
    public $_fileMask = 'order-split-%s-%s.csv';
    public $_runCommand = 2;
    public $_lastStack = array();
    public $_lastOrderId;
    public $_nextOrderFlag = false;
    public $_idColumn;
    public $_basePath;
    public $_increment = 0;
    public $_partsCsv = array();
    public $_mageUser = 'admin';
    public $_skipUntil = false;
    public $_skipCurrentOrder = false;
    public $_logFile = 'import_order_log_';
    public $_extraBuffer = array();

    public function __construct($file, $script, $idColumn, $limit = 200, $skipUntil = false)
    {
        $this->_filePath = $file;
        $this->_idColumn = $idColumn;
        $this->_limit = $limit;
        $this->_skipUntil = $skipUntil;
        $this->_basePath = getcwd();
        $this->_logFile = $script . '-' . $this->_logFile . date('Y-m-d_H-i-s') . '.log';

        $this->initMagento();
        $this->launchAdmin();
    }

    public function initMagento()
    {
        define('MAGENTO_ROOT', getcwd());
        require_once MAGENTO_ROOT . '/app/Mage.php';
        umask(0);

        Mage::app();
    }

    public function launchAdmin()
    {
        Mage::getSingleton('core/session', array('name' => 'adminhtml'));

        $request = Mage::app()->getRequest();
        $response = Mage::app()->getResponse();
        require_once 'app/code/community/Raveinfosys/Exporter/controllers/Adminhtml/ImporterController.php';
        $controller = new \Raveinfosys_Exporter_Adminhtml_ImporterController($request, $response);

        $user = Mage::getModel('admin/user')->loadByUsername($this->_mageUser);
        if (Mage::getSingleton('adminhtml/url')->useSecretKey()) {
            Mage::getSingleton('adminhtml/url')->renewSecretUrls();
        }
        $session = Mage::getSingleton('admin/session');
        $session->setIsFirstVisit(true);
        $session->setUser($user);
        $session->setAcl(Mage::getResourceModel('admin/acl')->loadAcl());
        Mage::dispatchEvent('admin_session_user_login_success', array('user' => $user));
    }

    public function array_combine_($keys, $values)
    {
        $result = array();
        foreach ($keys as $i => $k) {
            $result[$k][] = $values[$i];
        }
        array_walk($result, create_function('&$v', '$v = (count($v) == 1)? array_pop($v): $v;'));
        return    $result;
    }


    public function accumulateData($a, $useExtraBuffer = false)
    {
        $aH = array_combine($this->_headers, $a);
        if ($useExtraBuffer) {
            $this->_extraBuffer[] = $a;
        }
        if ($useExtraBuffer) {
            return ;
        }
        $id = @$aH[$this->_idColumn];
        if (is_string($id) && strlen($id) && 0 < intval($id)) {
            if ($this->_skipUntil && $this->_skipUntil > intval($id)) {
                echo 'Skip order #' . $id . PHP_EOL;
                $this->_skipCurrentOrder = true;
            } else {
                $this->_skipCurrentOrder = false;
            }
            $this->_nextOrderFlag = true;
        } else {
            $this->_nextOrderFlag = false;
            if (!$this->_skipCurrentOrder) {
                $this->_lastStack[] = $a;
            }
        }
        if ((count($this->_lastStack) >= $this->_limit) && $this->_nextOrderFlag) {
            echo 'Dumps ' . count($this->_lastStack) . ' orders ...' . PHP_EOL;
            $this->dumpNewPart();
        } else {
            if (!$this->_skipCurrentOrder) {
                $this->_lastStack[] = $a;
            }
        }
    }
    
    public function dumpNewPart()
    {
        // TODO Process left in stack orders
        $file = sprintf($this->_fileMask, $this->_increment++, date('d-m-Y_H-i-s'));
        $path = rtrim($this->_basePath, '\\/') . DS . trim($this->_saveDirectory, '\\/') . DS . trim($file, '\\/');

        $directory = pathinfo($path, PATHINFO_DIRNAME);
        if (!file_exists($directory)) {
            mkdir($directory);
        }

        $handle = fopen($path, 'w');
        $buffer = $this->_lastStack;
        array_unshift($buffer, $this->_headers);
        $result = false;
        foreach ($buffer as $row) {
            $result = fputcsv($handle, $row);
        }
        fclose($handle);

        if ($result) {
            $this->_lastStack = array();
            $this->_partsCsv[] = $path;
        } else {
            throw new Exception('http://storage1.static.itmages.com/i/17/0717/h_1500328525_2190527_fbb60f2e4d.pngUnable to save CSV part to file: ' . $file);
        }
        return true;
    }

    public function generateParts($useExtraBuffer = false)
    {
        echo '** GENERATE ** ' . date('d.m.Y H:i:s') . PHP_EOL;;
        if (file_exists($this->_filePath)) {
            $this->_fileHandle = fopen($this->_filePath, 'r');
            echo 'Generate orders from source file: ' . $this->_filePath;
            $csv = array();
            $this->_headers = fgetcsv($this->_fileHandle);
            // not working with \n
            // $csv = array_map('str_getcsv', file($this->_filePath));
            // array_shift($csv); # remove column header
            while (!feof($this->_fileHandle)) {
                $csv[] = fgetcsv($this->_fileHandle);
                if (!is_array($csv) || !count($csv)) {
                    return false;
                }
                array_walk($csv, function (&$a) use ($csv, $useExtraBuffer) {
                    //$a = array_combine($csv[0], $a);
                    $this->accumulateData($a, $useExtraBuffer);
                });
                $csv = array();
            }
            fclose($this->_fileHandle);
            return true;
        } else {
            return false;
        }
    }

    public function log($section, $string, $echoFlag = true)
    {
        $line = sprintf(
            '[%s] %s :: %s' . PHP_EOL,
            date('d.m.Y H:i:s'),
            $section,
            $string
        );
        $content = file_get_contents($this->_logFile);
        $content = $content . PHP_EOL . $line;
        file_put_contents($this->_logFile, $content);
        if ($echoFlag) {
            echo $line;
        }
    }

    public function loadFileForDiff($path)
    {
        $array = array();
        if (file_exists($path)) {
            $array = file($path);
            return true;
        } else {
            return false;
        }
    }

    public function retrieveColumnFromOrdersCollection($items, $column)
    {
        $result = array();
        foreach ($items as $index => $data) {
            if (is_object($data)) {
                $result[] = $data->getData($column);
            }
        }
        return $result;
    }

    public function processDiff($path)
    {
        echo '** PERFORM DIFF ** ' . date('d.m.Y H:i:s') . PHP_EOL;
        /** @var Mage_Sales_Model_Resource_Order_Collection  $ordersCollection */
        $ordersCollection = Mage::getModel('sales/order')->getCollection();
        $ordersCollection->addFieldToSelect('increment_id');
        $ordersInMagentoRaw = $ordersCollection->load()->getItems();
        $ordersToImportRaw = $this->_extraBuffer;

        $ordersInMagento = $this->retrieveColumnFromOrdersCollection($ordersInMagentoRaw, 'increment_id');
        $ordersToImport = array_column($ordersToImportRaw, 0);
        $ordersToImport = array_filter($ordersToImport);
        $collectedOrdersData = array();

        $diff = array_diff($ordersToImport, $ordersInMagento);
        if (is_array($diff) && count($diff)) {
            $collectedOrdersDataRaw = $this->collectImportOrdersById($diff);
            foreach ($collectedOrdersDataRaw as $item) {
                $collectedOrdersData[] = $item;
            }
            $this->dumpCsvSimple($collectedOrdersData, $path);
        }
    }

    public function dumpCsvSimple($buffer, $path)
    {
        $directory = pathinfo($path, PATHINFO_DIRNAME);
        if (!file_exists($directory)) {
            mkdir($directory);
        }

        $handle = fopen($path, 'w');
        array_unshift($buffer, $this->_headers);
        $result = false;
        foreach ($buffer as $row) {
            $result = fputcsv($handle, $row);
        }
        fclose($handle);

        if (!$result) {
            throw new Exception('Unable to save CSV part to file: ' . $path);
        }
        return true;
    }

    public function collectImportOrdersById($ids)
    {
        $result = array();
        // we use id as there is a mistake in extension: two columns customer_id
        $stillSameOrder = false;
        foreach ($this->_extraBuffer as $index => $item) {
            if (is_string($item[0]) && 1 < strlen($item[0])) {
                if (in_array($item[0], $ids)) {
                    $stillSameOrder = $item[0];
                    $result[] = $item;
                } else {
                    $stillSameOrder = false;
                }
            } elseif(!strlen($item[0]) && $stillSameOrder) {
                $result[] = $item;
            }
        }
        return $result;
    }

    public function processParts($singlePath)
    {
        $pathScript = __FILE__;
        if ($singlePath) {
            $this->_runCommand = 1;
            $csvFiles = array($singlePath);
        } else {
            $this->_runCommand = 2;
            $csvFiles = $this->_partsCsv;
        }
        // path & settings
        $data = array(
            'import_limit' => 9999999,
            'store_id' => 0
        );
        foreach ($csvFiles as $index => $path) {
            if (file_exists('stop_import_order.txt')) {
                $this->log('** EXIT ON STOP FILE **', 'Terminate before starting file: ' . $path);
                exit;
            }
            #$path = 'var/export/single_order_bad_serialize.csv';
            $this->log('** START FILE **', $path);
            switch ($this->_runCommand) {
                case 1:
                    $this->log('** PROCESS FILE **', $path, false);
                    try {
                        $csv = Mage::getModel('exporter/importorders')->readCSV($path , $data);
                    } catch (Exception $exception) {
                        var_dump($exception);
                    }
                    $this->log('** END FILE **', $path, false);
                break;
                case 2:
                    $this->log('** START CALL **', $path);
                    $call = 'php ' . $pathScript . ' ' . $path;
                    $buffer = '--';
                    if (! $buffer = shell_exec($call)) {
                        throw new Exception('Error while call: `' . $call .'` with buffer: ' . $buffer);
                    }
                    $this->log('** END CALL **', $path);
            }
        }
        // $this->log('** PROCESS **', 'END');
    }

    public function alignStates($dryRun = false)
    {
        $mode = 'delete'; // 're-save';
        /** @var Mage_Sales_Model_Resource_Order_Collection  $ordersCollection */
        $ordersCollection = Mage::getModel('sales/order')->getCollection();
        $ordersCollection->addFieldToSelect('increment_id');
        $ordersCollection->addFieldToSelect('status');
        $ordersCollection->addFieldToSelect('state');
        $ordersCollection->addFieldToSelect('entity_id');
        $ordersCollection->load();

        $counter = array();
        foreach ($this->_extraBuffer as $data) {
            $order = $ordersCollection->getItemByColumnValue('increment_id', $data[0]);
            if (is_object($order) && $order->getId()) {
                $saveOrderFlag = false;
                // idx of order_status = 46
                if ($order->getStatus() != $data[46]) {
                    $order->setStatus($data[46]);
                    $saveOrderFlag = true;
                }
                // idx of order_state = 47
                if ($order->getState() != $data[47]) {
                    $order->setState($data[47]);
                    $saveOrderFlag = true;
                }
                if ($saveOrderFlag) {
                    $counter[] = $order->getIncrementId();
                    if ($dryRun) {
                        $this->log('Check Status/State diff ::', 'Order #' . $order->getIncrementId() . ' is changed.');
                        continue;
                    }
                    switch ($mode) {
                        case 'delete':
                            $order->delete();
                            break;
                        case 're-save':
                            try {
                                if (!$order->getPayment()->hasMethodInstance() && $order->getPayment()->getMethod() === 'checkmo') {
                                    $instance = Mage::getModel('payment/method_checkmo');
                                    $instance->setInfoInstance($order->getPayment());
                                    $order->getPayment()->setMethodInstance($instance);
                                    $saveOrderFlag = false;
                                }
                                $order->save();
                            } catch (Exception $exception) {
                                print_r($exception->getMessage());
                            }
                            break;
                    }
                }
            }
        }
        echo 'Orders with aligned status/state: ' . PHP_EOL;
        print_r($counter);
    }
}

$generateParts = true;
$processDiff = false;
if (!isset($argv[2])) {
    // split & process orders from CSV
    if (isset($argv[1])) {
        $path = $argv[1];
        $generateParts = false;
    } else {
        // $path = 'var/export/order_export_20170717_194603.csv';
        $path = '';
    }
    // generate CSV only for diff orders
} else {
    $processDiff = $argv[1];
    $generateParts = true;
    $path = 'var/export/order_export_20170722_175417.csv';
}

$importer = new CSVSplitter($path, $generateParts ? 'manage' : 'process', 'order_id', 3000, 32904 /*26573*/ /*38476*/);

echo '* [START] *' . date('d.m.Y H:i:s') . PHP_EOL;

if ($generateParts) {
    // generate CSV parts
    $importer->generateParts($processDiff);
}
if ($processDiff) {
    // generate diff csv
    if ('1' === $argv[1]) {
        $importer->processDiff($argv[2]);
    } elseif ('2' === $argv[1]) {
        $importer->alignStates();
    }
} else {
    // perform import
    $importer->processParts($generateParts ? null : $path);
}

echo '* [FINISH] *' . date('d.m.Y H:i:s')  . PHP_EOL;