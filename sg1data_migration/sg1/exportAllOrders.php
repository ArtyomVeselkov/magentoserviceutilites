<?php

error_reporting(E_ALL | E_STRICT);

$user = 'dev';

define('MAGENTO_ROOT', getcwd());
require_once MAGENTO_ROOT . '/app/Mage.php';
umask(0);

Mage::app();
Mage::getSingleton('core/session', array('name'=>'adminhtml'));

$request = Mage::app()->getRequest();
$response = Mage::app()->getResponse();

require_once 'app/code/community/Raveinfosys/Exporter/controllers/Adminhtml/ExporterController.php';
$controller = new \Raveinfosys_Exporter_Adminhtml_ExporterController($request, $response);

$user = Mage::getModel('admin/user')->loadByUsername($user);
if (Mage::getSingleton('adminhtml/url')->useSecretKey()) {
    Mage::getSingleton('adminhtml/url')->renewSecretUrls();
}
$session = Mage::getSingleton('admin/session');
$session->setIsFirstVisit(true);
$session->setUser($user);
$session->setAcl(Mage::getResourceModel('admin/acl')->loadAcl());
Mage::dispatchEvent('admin_session_user_login_success', array('user' => $user));

echo 'START: ' . date('d.m.Y H:i:s') . PHP_EOL;
try {
    $controller->exportallAction();
} catch (Exception $exception) {
    var_dump($exception);
}
echo 'END: ' . date('d.m.Y H:i:s');