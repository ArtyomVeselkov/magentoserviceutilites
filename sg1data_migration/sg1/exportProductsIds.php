<?php

class ExportProductIds
{
    public $_logFile;
    public $_basePath;

    public function __construct($magentoUser, $logFile)
    {
        $this->_basePath = getcwd();
        $this->_logFile = $logFile . '-' . $this->_logFile . date('Y-m-d_H-i-s') . '.log';

        $this->initMagento();
        $this->launchAdmin();
    }

    public function initMagento()
    {
        define('MAGENTO_ROOT', getcwd());
        require_once MAGENTO_ROOT . '/app/Mage.php';
        umask(0);

        Mage::app();
    }

    public function launchAdmin()
    {
        Mage::getSingleton('core/session', array('name' => 'adminhtml'));

        $request = Mage::app()->getRequest();
        $response = Mage::app()->getResponse();
        require_once 'app/code/community/Raveinfosys/Exporter/controllers/Adminhtml/ImporterController.php';
        $controller = new Mage_Adminhtml_Controller_Action($request, $response);

        $user = Mage::getModel('admin/user')->loadByUsername($this->_mageUser);
        if (Mage::getSingleton('adminhtml/url')->useSecretKey()) {
            Mage::getSingleton('adminhtml/url')->renewSecretUrls();
        }
        $session = Mage::getSingleton('admin/session');
        $session->setIsFirstVisit(true);
        $session->setUser($user);
        $session->setAcl(Mage::getResourceModel('admin/acl')->loadAcl());
        Mage::dispatchEvent('admin_session_user_login_success', array('user' => $user));
    }

    public function getCurrentStoreProducts()
    {
        $result = array();
        /** @var Mage_Catalog_Model_Resource_Product_Collection  $ordersCollection */
        $ordersCollection = Mage::getModel('catalog/product')->getCollection();
        $result = $ordersCollection->load()->getItems();
        return $result;
    }

    public function getIdsForOuterProductsList($items)
    {
        $result = array();
        foreach ($items as $product) {
            /** @var Mage_Catalog_Model_Product $product */
            $sku = $product->getSku();
            $id = $product->getId();
            $result[$sku] = $id;
        }
        return $result;
    }

    public function saveCsv($file, $data)
    {
        $fileHandle = fopen($file, 'w');
        foreach ($data as $inc => $id) {
            fputcsv($fileHandle, array($inc, $id));
        }
        fclose($fileHandle);
    }

    public function export($file)
    {
        $products = $this->getCurrentStoreProducts();
        $result = $this->getIdsForOuterProductsList($products);
        $this->saveCsv($file, $result);
    }
}

$exportOrderIds = new ExportProductIds('admin', 'export.products.ids.log');
$exportOrderIds->export('export.products.ids.' . date('d-m-Y_H-i-s').'.csv');