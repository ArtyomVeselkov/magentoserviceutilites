<?php

$api_url_v1 = "https://www.migvapor.com/index.php/api/soap/?wsdl=1";

$username = '';
$password = '';

$params =  array(
    'trace' => 1,
    'exceptions' => true,
    'cache_wsdl' => WSDL_CACHE_NONE,
    );

$cli = new SoapClient($api_url_v1, $params);

//retrieve session id from login
try {
    $session_id = $cli->login($username, $password);
} catch (Exception $exception) {
    var_dump($exception);
}

//call customer.list method
$result = $cli->call($session_id, 'customer.list', array(array()));
